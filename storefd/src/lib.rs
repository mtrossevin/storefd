//! A library permitting interaction with the `LISTEN_FDS` and `NOTIFY_SOCKET` protocol.

#![cfg_attr(doc_cfg, feature(doc_cfg))]

#[cfg(feature = "listenfd")]
#[cfg_attr(doc_cfg, doc(cfg(feature = "listenfd")))]
pub use storefd_listen as listen;
#[cfg(feature = "notify")]
#[cfg_attr(doc_cfg, doc(cfg(feature = "notify")))]
pub use storefd_notify as notify;
